<?php
/**
 * Template Name: Press/Blog Page
 *
 * @package WordPress
 * @subpackage Charitas
 * @since Charitas 1.0
 */
?>

<?php get_header(); ?>
	<div class="item teaser-page-list">
		<div class="container_16">
			<aside class="grid_10">
				<h1 class="page-title"><?php the_title(); ?></h1>
			</aside>
			<div class="grid_6">
				<div id="rootline">
					<?php wplook_breadcrumbs(); ?>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>

	<div id="main" class="site-main container_16">
		<div class="inner">
			<div id="primary" class="grid_11 suffix_1">
				<?php get_template_part('loop', 'blog' ) ; ?>
			</div>

			<?php get_sidebar(); ?>
			<div class="clear"></div>
		</div>
	</div>
<?php get_footer(); ?>
<?php
/*
 * Plugin Name: About
*/

add_action('widgets_init', create_function('', 'return register_widget("wplook_about_widget");'));
class wplook_about_widget extends WP_Widget
{


    /*-----------------------------------------------------------------------------------*/
    /*	Widget actual processes
    /*-----------------------------------------------------------------------------------*/

    public function __construct()
    {
        parent::__construct(
            'wplook_about_widget',
            'WPlook About',
            array('description' => __('A widget for displaying about', 'wplook'),)
        );
    }
    
}
?>
<?php
/**
 * Template Name: Template-Doações
 */
?>

<?php get_header(); ?>
<?php

?>
<div class="item teaser-page-list">
    <div class="container_16">
        <aside class="grid_10">
            <h1 class="page-title"><?php the_title(); ?></h1>
        </aside>
        <div class="grid_6">
            <div id="rootline">
                <?php wplook_breadcrumbs(); ?>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div id="main" class="site-main container_16">

    <div class="inner">

        <div id="primary" class="grid_11 suffix_1"> <!-- Alteração Template Doações class="site-main container_16"-->

            <p>Para efetuar uma doação à Associação InPulsar poderá utilizar uma das seguintes formas:</p>

            <div class="paymend_detailes" style="display: block">
                <?php if ( ot_get_option('wpl_activate_paypal') == "activated") { ?>
                    <div class="toggle-content-donation">
                        <div class="expand-button"><p><?php _e('PayPal', 'wplook'); ?></p></div>
                        <div class="expand">
                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                <h2 class="donate-amoung"><?php _e('Donation amount', 'wplook'); ?></h2>



                                <?php if ( ot_get_option('wpl_default_first_amount') != "") { ?>
                                    <label>
                                        <input name="amount" type="radio" value="<?php echo ot_get_option('wpl_default_first_amount') ?>"><span><?php echo ot_get_option('wpl_curency_sign') ?><?php echo ot_get_option('wpl_default_first_amount') ?></span>
                                    </label>
                                <?php } ?>

                                <?php if ( ot_get_option('wpl_default_second_amount') != "") { ?>
                                    <label>
                                        <input name="amount" type="radio" value="<?php echo ot_get_option('wpl_default_second_amount') ?>"><span><?php echo ot_get_option('wpl_curency_sign') ?><?php echo ot_get_option('wpl_default_second_amount') ?></span>
                                    </label>
                                <?php } ?>

                                <?php if ( ot_get_option('wpl_default_third_amount') != "") { ?>
                                    <label>
                                        <input name="amount" type="radio" value="<?php echo ot_get_option('wpl_default_third_amount') ?>"><span><?php echo ot_get_option('wpl_curency_sign') ?><?php echo ot_get_option('wpl_default_third_amount') ?></span>
                                    </label>
                                <?php } ?>


                                <label>
                                    <input name="amount" type="number" min="<?php echo ot_get_option('wpl_min_amount') ?>" placeholder="<?php _e('Custom Amount in', 'wplook'); ?> <?php echo ot_get_option('wpl_curency_code') ?>" title="<?php _e('Custom Amount', 'wplook'); ?>">
                                </label>


                                <label>
                                    <input type="hidden" name="cmd" value="_xclick">
                                </label>


                                <label>
                                    <input type="hidden" name="business" value="<?php echo ot_get_option('wpl_paypal_email') ?>">
                                </label>

                                <label>
                                    <input type="hidden" name="item_name" value="Donation for INpulsar">
                                </label>
                                <label>
                                    <input type="hidden" name="item_number" value="<?php echo get_the_ID(); ?>">
                                </label>
                                <label>
                                    <input type="hidden" name="currency_code" value="<?php echo ot_get_option('wpl_curency_code') ?>">
                                </label>

                                <label>
                                    <input type="hidden" name="return" value="<?php echo get_page_link(ot_get_option('wpl_thank_you_page')); ?>">
                                </label>

                                <label>
                                    <input type="hidden" name="notify_url" value="<?php echo get_template_directory_uri() ?>/ipn.php" />
                                </label>

                                <label>
                                    <input class="donate_now_bt" type="submit" value="<?php _e('Donate !', 'wplook'); ?>" name="submit" alt="<?php _e('Make Donation with PayPal', 'wplook'); ?>">
                                </label>
                            </form>
                        </div>
                    </div>
                <?php } ?>

                <?php if ( ot_get_option('wpl_direct_bank_transfer') != "") { ?>
                    <div class="toggle-content-donation">
                        <div class="expand-button"><p><?php _e('Direct Bank Transfer', 'wplook'); ?></p></div>
                        <div class="expand"><?php echo ot_get_option('wpl_direct_bank_transfer') ?></div>
                    </div>
                <?php } ?>

                <?php if ( ot_get_option('wpl_cheque_payment') != "") { ?>
                    <div class="toggle-content-donation">
                        <div class="expand-button"><p><?php _e('Cheque Payment', 'wplook'); ?></p></div>
                        <div class="expand"><?php echo ot_get_option('wpl_cheque_payment') ?></div>
                    </div>
                <?php } ?>

            </div>

            <div>

                <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                    <?php the_content(); ?>
                <?php endwhile; // end of the loop. ?>

            </div>

        </div>

    </div>

</div>

<div class="clear"></div>

<?php get_footer(); ?>